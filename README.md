# OpenLDAP server role

Basic OpenLDAP server role for CentOS only.

Variables you can set in your playbook when including this role:

* ``ldap_suffix`` - Example: ``dc=ldap,dc=mydomain,dc=tld``
* ``ldap_admin_dn`` - Example: ``cn=ldapadmin,{{ ldap_suffix }}``
* ``ldap_admin_password`` - Example: ``{SSHA}xxx`` encrypted by slappasswd command

See the comments in ``defaults/main.yml`` for more info.
